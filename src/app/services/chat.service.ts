import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRoom, IMessage, IUser } from '../models';

@Injectable()
export class ChatService {
  constructor(private http: HttpClient) {}

  async login(login, password) {
    return await this.http.post<any>(`http://node55288-env-7352306.mycloud.by/user/login/${login}/${password}`, '').toPromise();
  }

  async getRooms(id: string) {
    return await this.http.get<IRoom[]>(`http://node55288-env-7352306.mycloud.by/room/user/${id}`).toPromise();
  }

  async getAllMesagges(id: string) {
    return await this.http.get<any>(`http://node55288-env-7352306.mycloud.by/history/room/${id}`).toPromise();
  }

  async updateUser(user: IUser) {
    return await this.http.put(`http://node55288-env-7352306.mycloud.by/user/`, user).toPromise();
  }

  async getLastMessages(messageId: string) {
    return await this.http.get<IMessage[]>(`http://node55288-env-7352306.mycloud.by/history/after/${messageId}`).toPromise();
  }

  async signUp(user: IUser) {
    return await this.http.post<any>(`http://node55288-env-7352306.mycloud.by/user/`, user).toPromise();
  }

  async getUserByUserName(user: string) {
    return await this.http.get<IUser>(`http://node55288-env-7352306.mycloud.by/user/username/${user}`).toPromise();
  }

  async createRoom(room: IRoom) {
    return await this.http.post(`http://node55288-env-7352306.mycloud.by/room/`, room).toPromise();
  }

}
