import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { IMessage } from '../models';

@Injectable()
export class WebsocketService {

  stompClient = null;
  handlers = [];

  constructor() { }

  public connect() {
    const socket = new SockJS('http://node55288-env-7352306.mycloud.by/poster');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, frame => {
      console.log('Connected: ' + frame);
      this.stompClient.subscribe('/topic/notifications', message => {
        this.handlers.forEach(handler => handler(JSON.parse(message.body)));
      });
    });
  }

  public addHandler(handler) {
    this.handlers.push(handler);
  }

  public disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  public sendMessage(message: IMessage) {
    this.stompClient.send('/app/send', {}, JSON.stringify(message));
  }
}
