import { Component, OnInit } from '@angular/core';
import { IUser } from './models';
import { ChatService } from './services/chat.service';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'poster-app';
  next: boolean;
  userLogin: string;
  password: string;
  response: any;
  user: any;
  error: boolean;
  newUser: boolean;
  newLogin: string;
  newPassword: string;
  newUsr: IUser;

  constructor(private chatSrv: ChatService) {}

  public ngOnInit() {
  }

  public login() {
    this.error = false;
    this.response = this.chatSrv.login(this.userLogin, this.password)
    .then(data => {
      if (data.message) {
        this.error = true;
      } else {
        this.user = data;
        this.next = true;
      }
    });
  }

  public signUp() {
    this.newUsr = {
      id: this.newLogin,
      username: this.newLogin,
      password: this.newPassword,
      avatarUrl: 'avatars/default.jpeg',
      lastOnline: null,
      online: ''
    };
    this.chatSrv.signUp(this.newUsr);
    this.response = this.chatSrv.login(this.newLogin, this.newPassword)
    .then(data => {
      if (data.message) {
        this.error = true;
      } else {
        this.user = data;
        this.next = true;
      }
    });
  }

  goNext() {
    this.next = true;
  }
}
