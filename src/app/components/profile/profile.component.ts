import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IUser } from 'src/app/models';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  @Input()
  currentUser: IUser;

  @Output()
  close: EventEmitter<any> = new EventEmitter();

  public user: IUser;

  constructor(private chatSrv: ChatService) {}

  async ngOnInit() {
    console.log(this.currentUser);
  }

  public selectUser() {
  }

  public update() {
    this.chatSrv.updateUser(this.currentUser);
  }
}
