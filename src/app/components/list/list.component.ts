import { Component, OnInit, Input } from '@angular/core';
import { IRoom, IUser } from 'src/app/models';
import { ChatService } from 'src/app/services/chat.service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  @Input()
  currentUser: IUser;

  id: string;
  public rooms: IRoom[];
  public user: boolean;
  public profile: boolean;
  public room: IRoom;
  public search: boolean;
  public searchUser: string;
  public findUser: IUser;
  public newRoom: IRoom;

  constructor(private chatSrv: ChatService, private websocketSrv: WebsocketService) {}

  async ngOnInit() {
    this.rooms = await this.chatSrv.getRooms(this.currentUser.id);
    this.websocketSrv.connect();
  }

  public selectRoom(room: IRoom) {
    this.room = room;
    this.user = this.user === true ? false : true;
  }

  public selectUserProfile() {
    this.profile = this.profile === true ? false : true;
  }

  async searchByName() {
    this.findUser = await this.chatSrv.getUserByUserName(this.searchUser);
  }

  async createRoom() {
    this.newRoom = {
      id: uuid(),
      name: 'New room',
      avatarUrl: 'avatars/default.jpeg',
      userIds: [
        this.currentUser.id,
        this.findUser.id
      ]
    };
    await this.chatSrv.createRoom(this.newRoom);
    this.rooms = await this.chatSrv.getRooms(this.currentUser.id);
    this.search = false;
  }
}
