import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { IRoom, IMessage, MessageType } from 'src/app/models';
import { v4 as uuid } from 'uuid';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {

  @Input()
  room: IRoom;

  @Input()
  id: string;

  public messages: IMessage[];
  public content: string;
  public message: IMessage;

  constructor(private chatSrv: ChatService, private socketSrv: WebsocketService) { }

  async ngOnInit() {
    this.content = '';
    const msg = await this.chatSrv.getAllMesagges(this.room.id);
    if (msg.status === 200) {
      this.messages = null;
    } else {
      this.messages = await this.chatSrv.getAllMesagges(this.room.id);
    }
    console.log(this.messages);
  }

  async send() {
    this.message = {
      id: uuid(),
      content: this.content,
      roomId: this.room.id,
      senderId: this.id,
      messageType: MessageType.Text,
      sendAt: new Date
    };
    await this.socketSrv.sendMessage(this.message);
    await this.getLastMessages();
  }

  async getLastMessages() {
    if (!this.messages) {
      const msg = await this.chatSrv.getAllMesagges(this.room.id);
      this.messages = msg;
    } else {
      const msg = await this.chatSrv.getLastMessages(this.messages[this.messages.length - 1].id);
      msg.map(mes => {
        this.messages.push(mes);
      });
    }
  }
}
