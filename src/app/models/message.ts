export enum MessageType {
  System = 'SYSTEM',
  Text = 'TEXT',
  Document = 'DOCUMENT'
}

export interface IMessage {
  id: string;
  content: string;
  roomId: string;
  senderId: string;
  messageType: MessageType;
  sendAt: Date;
}
