import { IUser } from './user';

export interface IRoom {
  id: string;
  name: string;
  avatarUrl?: string;
  userIds: string[];
}
