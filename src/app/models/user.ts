export interface IUser {
  id: string;
  username: string;
  password: string;
  avatarUrl?: string;
  lastOnline: string;
  online: string;
}
