export interface INotification {
  userId: string;
  roomId: string;
  content: string;
  notificationAt: Date;
}
